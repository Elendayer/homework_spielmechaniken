﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UEGP3CA
{
    public class PlaceBuilding : MonoBehaviour
    {
        private Grid _grid;

        private int _distance = 2000;
        private LayerMask _layerMask;

        private Shader _shader;
        private BuildingScriptableObject _buildingScriptableObjectScript;
        private GameObject _buildingLayer;

        private bool _isColliding = false;

        private MeshRenderer _meshRenderer;
        private Material _previewMaterial;

        public BuildingScriptableObject BuildingScriptableObjectScript
        {
            get => _buildingScriptableObjectScript;
            set => _buildingScriptableObjectScript = value;
        }

        private void Awake()
        {
            _layerMask = LayerMask.GetMask("Ground");

            _shader = Shader.Find("Standard");
            _grid = FindObjectOfType<Grid>();
            _buildingLayer = GameObject.FindWithTag("BuildingLayer");

            Rigidbody rigidbody;
            rigidbody = gameObject.AddComponent<Rigidbody>();
            rigidbody.useGravity = false;
            rigidbody.freezeRotation = true;

            _meshRenderer = GetComponent<MeshRenderer>();
            _previewMaterial = new Material(_shader);
            _previewMaterial.color = Color.green;

            _meshRenderer.material = _previewMaterial;
        }

        public void Update()
        {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hitInfo, _distance, _layerMask);

            if (hitInfo.collider != null)
            {
                MoveWithMouse(hitInfo.point);
            }

            if (_isColliding == false)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    PlaceBuildingCloseToPoint(hitInfo.point, _buildingScriptableObjectScript.BuildingObject);
                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                Destroy(this.gameObject);
            }
        }

        private void PlaceBuildingCloseToPoint(Vector3 closetPoint, GameObject building)
        {
            var finalPosition = _grid.GetClosestPointOnGrid(closetPoint);

            GameObject newBuilding = Instantiate(building, _buildingLayer.transform);
            newBuilding.transform.position = finalPosition;
            Destroy(this.gameObject);
        }

        public void MoveWithMouse(Vector3 closetPoint)
        {
            var finalPosition = _grid.GetClosestPointOnGrid(closetPoint);
            this.transform.position = finalPosition;
        }

        private void OnCollisionStay(Collision other)
        {
            if (other.gameObject.layer != 11)
            {
                _isColliding = true;
                _previewMaterial.color = Color.red;
            }
        }
        private void OnCollisionExit(Collision other)
        {
            if (other.gameObject.layer != 11)
            {
                _isColliding = false;
                _previewMaterial.color = Color.green;
            }
        }
    }
}
    
    
    
