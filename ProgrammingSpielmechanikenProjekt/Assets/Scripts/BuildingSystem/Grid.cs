﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UEGP3CA
{
    public class Grid : MonoBehaviour
    {
        [SerializeField] private int worldGridSize;

        private float _size = 1f;

        public Vector3 GetClosestPointOnGrid(Vector3 position)
        {
            position -= transform.position;

            int xCount = Mathf.RoundToInt(position.x / _size);
            int yCount = Mathf.RoundToInt(position.y / _size);
            int zCount = Mathf.RoundToInt(position.z / _size);

            Vector3 result = new Vector3
            (
                (float) xCount * _size,
                (float) yCount * _size,
                (float) zCount * _size
            );

            result += transform.position;

            return result;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            for (int x = 0; x < worldGridSize; x++)
            {
                for (int z = 0; z < worldGridSize; z++)
                {
                    var point = GetClosestPointOnGrid(new Vector3(x, 0f, z));
                    Gizmos.DrawSphere(point, 0.1f);
                }
            }
        }
    }
}
