﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UEGP3CA
{
    public class PreviewBuilding : MonoBehaviour
    {
        private GameObject _buildingLayer;
        
        [SerializeField] private BuildingScriptableObject buildingScriptableObject;

        public BuildingScriptableObject BuildingScriptableObjectScript
        {
            get => buildingScriptableObject;
            set => buildingScriptableObject = value;
        }
        private void Awake()
        {
            _buildingLayer = GameObject.FindWithTag("BuildingLayer");
        }

        public void SpawnPreviewBuilding()
        {
            GameObject previewBuilding = Instantiate(buildingScriptableObject.BuildingObject, _buildingLayer.transform);
            PlaceBuilding placeBuilding = previewBuilding.AddComponent<PlaceBuilding>();
            placeBuilding.BuildingScriptableObjectScript = buildingScriptableObject;
            placeBuilding.gameObject.layer = 12;
        }
    }
}
