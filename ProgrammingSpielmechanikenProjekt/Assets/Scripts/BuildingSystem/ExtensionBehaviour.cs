﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace UEGP3CA
{
    public class ExtensionBehaviour : MonoBehaviour
    {
        private Extension _extension;

        public Extension Extension => _extension;
        
    }
}
