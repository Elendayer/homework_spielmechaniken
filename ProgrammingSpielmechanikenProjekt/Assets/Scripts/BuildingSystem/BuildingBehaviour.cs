﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UEGP3CA;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace UEGP3CA
{
    public class BuildingBehaviour : MonoBehaviour
    {
        private List<ScriptableObject> _buildingQueue = new List<ScriptableObject>();

        private ScriptableObject[] _buyables;

        protected private UiManager _uiManager;

        [SerializeField] private BuildingScriptableObject buildingScriptableObject;

        [SerializeField] private bool _isSelected = false;

        private float _remainingTime;
        private TextMeshProUGUI _text;
        private float _interval = 0.1f;

        private bool _isRunning;

        public bool IsSelected
        {
            get => _isSelected;
            set => _isSelected = value;
        }

        private void Awake()
        {
            _uiManager = FindObjectOfType<UiManager>();
        }

        private void Start()
        {
            Array.Resize(ref _buyables,
                buildingScriptableObject.Units.Length + buildingScriptableObject.Upgrades.Length +
                buildingScriptableObject.Extensions.Length);

            for (int i = 0; i < buildingScriptableObject.Units.Length; i++)
            {
                _buyables[i] = buildingScriptableObject.Units[i];
            }

            for (int i = 0; i < buildingScriptableObject.Upgrades.Length; i++)
            {
                _buyables[i + buildingScriptableObject.Units.Length] = buildingScriptableObject.Upgrades[i];
            }

            for (int i = 0; i < buildingScriptableObject.Extensions.Length; i++)
            {
                _buyables[i + buildingScriptableObject.Units.Length + buildingScriptableObject.Upgrades.Length] =
                    buildingScriptableObject.Extensions[i];
            }
        }

        public void Selected()
        {
            _uiManager.ActivateSelectedBuilding();

            if (_isSelected == false)
            {
                MakeBuyButtons();
                if (_buildingQueue.Count > 0)
                {
                    MakeQueueButtons();
                    DetermineTypeofScriptableObject();

                }

                _isSelected = true;
            }
        }

        public void AddToBuildingQueue(ScriptableObject elementToAdd)
        {
            _buildingQueue.Add(elementToAdd);
            _buildingQueue.TrimExcess();

            _uiManager.CreateSelectedBuildingQueueButton(this, _buildingQueue.Last());
            DetermineTypeofScriptableObject();
            _text = _uiManager.QueueButtonsLayer.transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>();
        }

        public void RemoveFromBuildingQueue(ScriptableObject scriptableObject)
        {
            _buildingQueue.Remove(scriptableObject);
            _buildingQueue.TrimExcess();
        }

        private void MakeQueueButtons()
        {
            for (int i = 0; i < _buildingQueue.Count; i++)
            {
                _uiManager.CreateSelectedBuildingQueueButton(this, _buildingQueue.ElementAt(i));
            }

            Debug.Log(_uiManager.QueueButtonsLayer.transform.GetChild(0));
            _text = _uiManager.QueueButtonsLayer.transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>();
        }

        void MakeBuyButtons()
        {
            if (_buyables != null)
            {
                foreach (ScriptableObject buyable in _buyables)
                {
                    _uiManager.CreateSelectedBuildingBuyButton(this, buyable);
                }
            }
        }

        void DetermineTypeofScriptableObject()
        {
            if (_isRunning == false)
            {
                float buildTime = 0;

                _text = _uiManager.QueueButtonsLayer.transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>();

                if (_buildingQueue.Count >= 1)
                {
                    if (_buildingQueue[0] is Unit)
                    {
                        Unit unit = _buildingQueue[0] as Unit;
                        if (unit != null)
                        {
                            buildTime = unit.BuildTime;
                        }
                    }

                    if (_buildingQueue[0] is Upgrade)
                    {
                        Upgrade upgrade = _buildingQueue[0] as Upgrade;
                        if (upgrade != null)
                        {
                            buildTime = upgrade.BuildTime;
                        }
                    }

                    if (_buildingQueue[0] is Extension)
                    {
                        BuildExtension();
                    }

                    StartCoroutine(Countdown(buildTime));
                }
            }
        }

        IEnumerator Countdown(float _buildTime)
        {
            _isRunning = true;
            _remainingTime = _buildTime;

            while (_remainingTime >= 0)
            {
                _remainingTime -= _interval;

                if (_isSelected == true && _text != null)
                {
                    _text.text = _remainingTime.ToString("F1");
                }

                yield return new WaitForSeconds(_interval);

                if (_remainingTime <= 0)
                {
                    _isRunning = false;
                    Spawn();
                    yield break;
                }
            }
        }

        void Spawn()
        {
            if (_buildingQueue[0] is Unit)
            {
                Unit unit = _buildingQueue[0] as Unit;
                GameObject newUnit = Instantiate(unit.UnitPrefab, _uiManager.UnitLayer.transform);
                newUnit.name = unit.UnitName;
                newUnit.transform.position = gameObject.transform.position;
                UnitBehaviour unitBehaviour = newUnit.GetComponent<UnitBehaviour>();
                unitBehaviour.ThisUnit = unit;
                unitBehaviour.FindSpawnLocation(gameObject);

            }

            else if (_buildingQueue[0] is Upgrade)
            {
                Debug.Log("TBA - Upgrades");
            }

            if (_buildingQueue.Count >= 0)
            {
                _buildingQueue.RemoveAt(0);
                _buildingQueue.TrimExcess();
                _uiManager.DeleteFirstInQueue();
                DetermineTypeofScriptableObject();
            }
        }

        void BuildExtension()
        {
            Extension extension = _buildingQueue[0] as Extension;
            GameObject newExtension = Instantiate(extension.ExtensionPrefab, gameObject.transform);
            PlaceExtension placeExtension = newExtension.AddComponent<PlaceExtension>();
            placeExtension.Extension = _buildingQueue[0] as Extension;
        }
    }
}