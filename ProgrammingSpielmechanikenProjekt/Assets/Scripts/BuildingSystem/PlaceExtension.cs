﻿using UEGP3CA;
using UnityEngine;

namespace UEGP3CA
{
    public class PlaceExtension : MonoBehaviour
    {
        private Grid _grid;

        private int _distance = 2000;
        private LayerMask _layerMask;

        private Shader _shader;

        private GameObject _creatingBuilding;

        private bool _isColliding = false;
        private bool _isConnected = false;
            

        private MeshRenderer _meshRenderer;
        private Material _previewMaterial;

        private float _lowerX;
        private float _upperX;
        private float _lowerZ;
        private float _upperZ;

        public Extension Extension { get; set; }

        private Vector3 _finalPosition;
        private void Awake()
        {
            _layerMask = LayerMask.GetMask("Ground");

            _shader = Shader.Find("Standard");
            _grid = FindObjectOfType<Grid>();
            _creatingBuilding = gameObject.transform.parent.gameObject;

            Rigidbody rigidbody;
            rigidbody = gameObject.AddComponent<Rigidbody>();
            rigidbody.useGravity = false;
            rigidbody.freezeRotation = true;

            _meshRenderer = GetComponent<MeshRenderer>();
            _previewMaterial = new Material(_shader);
            _previewMaterial.color = Color.green;

            _meshRenderer.material = _previewMaterial;

            var position = transform.parent.transform.position;
            var localScaleOfCreator = _creatingBuilding.transform.localScale;
            _lowerX = position.x + 0.5f - 0.5f * localScaleOfCreator.x;
            _upperX = position.x + 0.5f + 0.5f * localScaleOfCreator.x;
            _lowerZ = position.z + 0.5f - 0.5f * localScaleOfCreator.z;
            _upperZ = position.z + 0.5f + 0.5f * localScaleOfCreator.z;

            var transformLocalScale = gameObject.transform.localScale;
            transformLocalScale.x = 0.33f;
            transformLocalScale.z = 0.33f;
        }

        public void Update()
        {
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hitInfo, _distance, _layerMask);

            if (hitInfo.collider != null)
            {
                MoveWithMouse(hitInfo.point);
            }
            if (_isColliding == false && _isConnected == true)
            {
                _previewMaterial.color = Color.green;
                if (Input.GetMouseButtonDown(0))
                {
                    PlaceBuildingCloseToPoint(hitInfo.point, Extension.ExtensionPrefab);
                }
            }
            else
            {
                _previewMaterial.color = Color.red;
            }
            if (Input.GetMouseButtonDown(1))
            {
                Destroy(this.gameObject);
            }
            CheckDistanceToParent();
        }

        private void PlaceBuildingCloseToPoint(Vector3 closetPoint, GameObject building)
        {
            GameObject newExtension = Instantiate(building, _creatingBuilding.transform);
            newExtension.transform.position = _finalPosition;
            Destroy(this.gameObject);
        }

        private void MoveWithMouse(Vector3 closetPoint)
        {
            _finalPosition = _grid.GetClosestPointOnGrid(closetPoint);
            _finalPosition.x -= 0.5f;
            _finalPosition.z -= 0.5f;
            gameObject.transform.position = _finalPosition;
        }

        private void CheckDistanceToParent()
        {
            Vector3 rectPoint = new Vector3(_finalPosition.x,_finalPosition.z,0);
            Rect rect = new Rect {xMin = _lowerX - 1, xMax = _upperX + 1, yMin = _lowerZ - 1, yMax = _upperZ + 1};

            if(rect.Contains(rectPoint))
            {
                _isConnected = true;
            }
            else
            {
                _isConnected = false;
            }
        }
        private void OnCollisionStay(Collision other)
        {
            if (other.gameObject.layer != 11)
            {
                _isColliding = true;
            }
        }
        private void OnCollisionExit(Collision other)
        {
            if (other.gameObject.layer != 11)
            {
                _isColliding = false;
            }
        }
    }
}

