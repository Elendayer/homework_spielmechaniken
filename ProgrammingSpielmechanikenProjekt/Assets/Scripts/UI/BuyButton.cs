﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UEGP3CA
{
    public class BuyButton : MonoBehaviour
    {
        private BuildingBehaviour _buildingBehaviour;
        private ScriptableObject _thingScriptableObject;

        public BuildingBehaviour BuildingBehaviour
        {
            get => _buildingBehaviour;
            set => _buildingBehaviour = value;
        }

        public ScriptableObject ThingScriptableObject
        {
            get => _thingScriptableObject;
            set => _thingScriptableObject = value;
        }

        public void UseButton()
        {
            _buildingBehaviour.AddToBuildingQueue(_thingScriptableObject);
        }
    }
}
