﻿using System;
using System.Collections;
using System.Collections.Generic;
using UEGP3CA;
using TMPro;
using UnityEngine;

public class QueueButton : MonoBehaviour
{
    private BuildingBehaviour _buildingBehaviour;
    private ScriptableObject _thingScriptableObject;

    public BuildingBehaviour BuildingBehaviour
    {
        get => _buildingBehaviour;
        set => _buildingBehaviour = value;
    }

    public ScriptableObject ThingScriptableObject
    {
        get => _thingScriptableObject;
        set => _thingScriptableObject = value;
    }

    public void UseButton()
    {
        _buildingBehaviour.RemoveFromBuildingQueue(_thingScriptableObject);
        Destroy(this.gameObject);
    }
}
