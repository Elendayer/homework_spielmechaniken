﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UEGP3CA;
using UnityEngine;

namespace UEGP3CA
{
    public class UiManager : MonoBehaviour
    {
        [HeaderAttribute("MainLayers")] 
        [SerializeField] private GameObject _mainBuildingButtons;
        [SerializeField] private GameObject _selectedBuildingButtons;
        [SerializeField] private GameObject _unitLayer;

        [HeaderAttribute("SelectedBuildingLayers")] 
        [SerializeField] private GameObject _buyButtonsLayer;
        [SerializeField] private GameObject _queueButtonsLayer;

        [HeaderAttribute("Buttons")]
        [SerializeField] private GameObject _buyButton;
        [SerializeField] private GameObject _queueButton;
        public GameObject UnitLayer
        {
            get => _unitLayer;
            set => _unitLayer = value;
        }
        public GameObject BuyButtonsLayer
        {
            get => _buyButtonsLayer;
            set => _buyButtonsLayer = value;
        }
        public GameObject QueueButtonsLayer
        {
            get => _queueButtonsLayer;
            set => _queueButtonsLayer = value;
        }
        private void Awake()
        {
            _selectedBuildingButtons.SetActive(false);
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && _selectedBuildingButtons.activeSelf)
            {
                ActivateMainBuilding();
            }
        }
        public void ActivateSelectedBuilding()
        {
            _mainBuildingButtons.SetActive(false);
            _selectedBuildingButtons.SetActive(true);
        }
        private void ActivateMainBuilding()
        {
            DeleteSelectedBuildingButtons();
            _mainBuildingButtons.SetActive(true);
            _selectedBuildingButtons.SetActive(false);
        }
        public void CreateSelectedBuildingBuyButton(BuildingBehaviour buildingBehaviour, ScriptableObject thingsToBuy)
        {
            GameObject currentBuyButtonObj = Instantiate(_buyButton, _buyButtonsLayer.transform);
            BuyButton currentBuyButton = currentBuyButtonObj.GetComponent<BuyButton>();
            currentBuyButton.BuildingBehaviour = buildingBehaviour;
            currentBuyButton.ThingScriptableObject = thingsToBuy;
            currentBuyButton.GetComponentInChildren<TextMeshProUGUI>().text = thingsToBuy.name;
        }
        public void CreateSelectedBuildingQueueButton(BuildingBehaviour buildingBehaviour,
            ScriptableObject thingToQueue)
        {
            GameObject currentQueueButtonObj = Instantiate(_queueButton, _queueButtonsLayer.transform);
            QueueButton currentQueueButton = currentQueueButtonObj.GetComponent<QueueButton>();
            currentQueueButton.BuildingBehaviour = buildingBehaviour;
            currentQueueButton.ThingScriptableObject = thingToQueue;
        }
        public void DeleteFirstInQueue()
        {
            Destroy(_queueButtonsLayer.transform.GetChild(0).gameObject);
        }
        public void DeleteSelectedBuildingButtons()
        {
            _buyButtonsLayer.GetComponentInChildren<BuyButton>().BuildingBehaviour.IsSelected = false;
            
            foreach (Transform child in _buyButtonsLayer.transform)
            {
                Destroy(child.gameObject);
            }
            foreach (Transform child in _queueButtonsLayer.transform)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
