﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UEGP3CA
{
    public class UnitBehaviour : MonoBehaviour
    {
        private Unit _thisUnit;
        private LayerMask _layerMask;
        [SerializeField] private GameObject spawnProbe;
        public Unit ThisUnit
        {
            get => _thisUnit;
            set => _thisUnit = value;
        }

        private void Awake()
        {
            _layerMask = LayerMask.GetMask("Building", "Unit");
        }

        public void FindSpawnLocation(GameObject creatingBuilding)
        {
            var position = transform.position;
            var localScaleOfCreator = creatingBuilding.transform.localScale;
            float lowerX = position.x + 0.5f - 0.5f * localScaleOfCreator.x;
            float upperX = position.x + 0.5f + 0.5f * localScaleOfCreator.x;
            float lowerZ = position.z - 0.5f - 0.5f * localScaleOfCreator.z;
            float upperZ = position.z + 0.5f + 0.5f * localScaleOfCreator.z;

            
            for (float x = lowerX; x < upperX; x++)
            {
                var thisTransform = transform;
                thisTransform.position = new Vector3(x,0, lowerZ);

                var hitColliders = Physics.OverlapSphere(thisTransform.position, 0.4f, _layerMask);
                if (hitColliders.Length == 0)
                {
                    return;
                }
            }
            
            for (float z = lowerZ; z < upperZ; z++)
            {
                var thisTransform = transform;
                thisTransform.position = new Vector3(upperX,0, z);

                var hitColliders = Physics.OverlapSphere(thisTransform.position, 0.4f, _layerMask);
                if (hitColliders.Length == 0)
                {
                    return;
                }
            }
            for (float x = upperX; x > lowerX-1; x--)
            {
                var thisTransform = transform;
                thisTransform.position = new Vector3(x,0, upperZ);

                var hitColliders = Physics.OverlapSphere(thisTransform.position, 0.4f, _layerMask);
                if (hitColliders.Length == 0)
                {
                    return;
                }
            }
            for (float z = upperZ; z > lowerZ-1; z--)
            {
                var thisTransform = transform;
                thisTransform.position = new Vector3(lowerX-1,0, z);

                var hitColliders = Physics.OverlapSphere(thisTransform.position, 0.4f, _layerMask);
                if (hitColliders.Length == 0)
                {
                    return;
                }
            }
            Debug.Log("no possible spawn location");
            Destroy(gameObject);
        }
    }
}

