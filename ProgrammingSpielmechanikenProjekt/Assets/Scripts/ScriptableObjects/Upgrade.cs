﻿using UnityEngine;

namespace UEGP3CA
{
    [CreateAssetMenu(fileName = "new UpgradeScriptableObject", menuName = "ScriptableObjects/Upgrade")]
    public class Upgrade : ScriptableObject
    {
        [SerializeField] private string _upgradeName;
        [SerializeField] private float _buildTime;

        public string UpgradeName
        {
            get => _upgradeName;
            set => _upgradeName = value;
        }
        public float BuildTime
        {
            get => _buildTime;
            set => _buildTime = value;
        }
    }
}
