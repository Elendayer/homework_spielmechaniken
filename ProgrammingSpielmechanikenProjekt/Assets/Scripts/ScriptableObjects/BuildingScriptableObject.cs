﻿using System.Collections;
using System.Collections.Generic;
using UEGP3CA;
using UnityEngine;

[CreateAssetMenu(fileName = "new BuildingScriptableObject", menuName = "ScriptableObjects/BuildingScriptableObject")]
public class BuildingScriptableObject : ScriptableObject
{
    
    [SerializeField] private string buildingName;
    [SerializeField] private GameObject buildingObject;

    [SerializeField] private Unit[] units;
    [SerializeField] private Upgrade[] upgrades;
    [SerializeField] private Extension[] extensions;

    public GameObject BuildingObject => buildingObject;
    public string BuildingName => buildingName;
    public Unit[] Units => units;
    public Upgrade[] Upgrades => upgrades;
    public Extension[] Extensions => extensions;
    
    
}
