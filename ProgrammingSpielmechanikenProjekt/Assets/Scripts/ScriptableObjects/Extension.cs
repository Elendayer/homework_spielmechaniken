﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UEGP3CA
{
    [CreateAssetMenu(fileName = "new ExtensionScriptableObject", menuName = "ScriptableObjects/Extension")]
    public class Extension : ScriptableObject
    {
        [SerializeField] private string _extensionName;
        [SerializeField] private float _buildTime;
        [SerializeField] private GameObject _extensionPrefab;

        public string ExtensionName
        {
            get => _extensionName;
            set => _extensionName = value;
        }
        public float BuildTime
        {
            get => _buildTime;
            set => _buildTime = value;
        }
        public GameObject ExtensionPrefab
        {
            get => _extensionPrefab;
            set => _extensionPrefab = value;
        }
    }
}