﻿using UnityEngine;

namespace UEGP3CA
{
    [CreateAssetMenu(fileName = "new UnitScriptableObject", menuName = "ScriptableObjects/Unit")]
    public class Unit : ScriptableObject
    {
       [SerializeField] private string _unitName;
       [SerializeField] private float _buildTime;
       [SerializeField] private GameObject _unitPrefab;

        public string UnitName
        {
            get => _unitName;
            set => _unitName = value;
        }
        public float BuildTime
        {
            get => _buildTime;
            set => _buildTime = value;
        }
        public GameObject UnitPrefab
        {
            get => _unitPrefab;
            set => _unitPrefab = value;
        }
    }
}
