﻿using System;
using TMPro;
using UnityEngine;

namespace UEGP3CA
{
    public class SelectObject : MonoBehaviour
    {
        [SerializeField] private int distance;

        private LayerMask _layerMask;
        private UiManager _uiManager;
        private void Awake()
        {
            _uiManager =  _uiManager = FindObjectOfType<UiManager>();
            _layerMask = LayerMask.GetMask("Building", "Unit");
        }
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, distance, _layerMask))
                {
                    if (hit.collider.gameObject.layer == 9)
                    {
                        BuildingBehaviour buildingBehaviour = hit.collider.gameObject.GetComponent<BuildingBehaviour>();
                        if (buildingBehaviour != null)
                        {
                            if (_uiManager.BuyButtonsLayer.transform.childCount > 0)
                            {
                                _uiManager.DeleteSelectedBuildingButtons();
                            }
                            buildingBehaviour.Selected();
                        }
                    }
                }
            }
        }
    }
}
